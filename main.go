package main

import (
	"flag"	
	"html/template"
	"net/http"		
	"github.com/gorilla/mux"	
)

type results struct {
    Metadata []metadata_info
}

type metadata_info struct {
    Results []results_info
    //Attr  []attr_info
}

type results_info struct {
    id       int   
}


func main() {
	

	flag.Parse()	
	 r := mux.NewRouter().StrictSlash(true)
	r.HandleFunc("/", http.HandlerFunc(index))
	r.HandleFunc("/products", http.HandlerFunc(products))
	http.Handle("/", r)
	http.Handle("/css/", http.StripPrefix("/css/", http.FileServer(http.Dir("css"))))
	http.Handle("/images/", http.StripPrefix("/images/", http.FileServer(http.Dir("images"))))
 
	http.ListenAndServe(":8007", nil)

}

func index(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "text/html; charset=utf-8")
	if r.Method != "GET" || r.URL.Path != "/" {
		http.NotFound(w, r)
		return
	}

	

	pageTemplate, err := template.ParseFiles("tpl/index.html", "tpl/header.html", "tpl/bar.html", "tpl/footer.html")
	if err != nil {
		http.NotFound(w, r)
		return
	}

	tplValues := map[string]interface{}{"Header": "Demo", "Copyright": "Demo App"}
	

	pageTemplate.Execute(w, tplValues)
	if err != nil {
		http.NotFound(w, r)
		return
	}
}



func products(w http.ResponseWriter, r *http.Request) {
	
	tplValues := map[string]interface{}{"Header": "Products", "Copyright": "Demo App" }


levels, err := getProducts()
tplValues["levels"] = levels
	if err != nil {
		http.NotFound(w, r)
		return
	}



	pageTemplate, err := template.ParseFiles("tpl/products.html", "tpl/header.html", "tpl/bar.html", "tpl/footer.html")
	if err != nil {
		http.NotFound(w, r)
		return
	}

	

	err = pageTemplate.Execute(w, tplValues)
	if err != nil {
		http.NotFound(w, r)
		return
	}
}


func getProducts() ([]map[string]string, error) {
levels := []map[string]string{}
levels = append(levels, map[string]string{"title": "title5", "description": "description", "price": "price", "quantity": "quantity", "filename": "kingone-7023-954221-1-catalog.jpg"})
levels = append(levels, map[string]string{"title": "title5", "description": "description", "price": "price", "quantity": "quantity", "filename": "lg-6248-364531-1-catalog.jpg"})
levels = append(levels, map[string]string{"title": "title5", "description": "description", "price": "price", "quantity": "quantity", "filename": "lg-8710-844231-1-catalog.jpg"})
levels = append(levels, map[string]string{"title": "title5", "description": "description", "price": "price", "quantity": "quantity", "filename": "peepvn-5821-915132-1-catalog.jpg"})
levels = append(levels, map[string]string{"title": "title5", "description": "description", "price": "price", "quantity": "quantity", "filename": "tech-box-3452-793502-1-catalog.jpg"})
	
return levels, nil
}
